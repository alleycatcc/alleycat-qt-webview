set -eu
set -o pipefail
set -x

catqt-vars-common () {

    set -x

    qmldirroot="$catqt_build_rootdir"/qml
    themedirroot="$catqt_build_rootdir"/themes
    qmakedir="$catqt_build_rootdir"/qmake
    sharedlib=lib"$catqt_plugin_projectname".so


    # --- the stub is necessary to make rm -rf safer.
    catqt_builddirstub=build
    catqt_builddirfull="$catqt_build_builddir"/"$catqt_builddirstub"
    catqt_builddirresourcesstub=resources
    catqt_builddirresources="$catqt_builddirfull"/"$catqt_builddirresourcesstub"
    catqt_themedir="$themedirroot"/"$catqt_plugin_theme"-"$catqt_plugin_themever"
    catqt_qmldir="$qmldirroot"/"$catqt_plugin_qmltype"-"$catqt_plugin_qmlver"

    set +x
}

catqt-vars-init () {
    fun catqt-vars-common

    set -x
    set +x
}

catqt-vars-make () {
    fun catqt-vars-common

    set -x

    catqt_srcdir="$catqt_build_rootdir"/src
    catqt_qrcdirroot="$catqt_build_rootdir"/qrc

    catqt_qtqtdir="$catqt_plugin_qtdir"

    # catqt_qtcreator="$catqt_plugin_qtrootdir"/Tools/QtCreator/bin/qtcreator

    catqt_qmakefilepri="$qmakedir"/deployment.pri
    catqt_qmakefileproorig="$qmakedir"/main.pro
    catqt_qmakefileprobuild="$catqt_builddirfull"/"$catqt_plugin_projectname".pro

    catqt_shadowbuilddirroot="$catqt_build_builddir"

    catqt_desktop_makecmd=make

    catqt_deployjson=android-"$sharedlib"-deployment-settings.json

    catqt_srcfiles=(
        "$catqt_srcdir"/catqt_global.h
        "$catqt_srcdir"/catqt_main.h
        "$catqt_srcdir"/catqt_main.cpp
        "$catqt_srcdir"/catqt_qml.h
        "$catqt_srcdir"/catqt_qml.cpp
    )

    catqt_projfile="$catqt_builddirfull"/"$catqt_plugin_projectname".pro

    if [ "$catqt_build_target" = android ]; then
        catqt_androidsrc="$catqt_srcdir"/android-sources
        catqt_gradleproperties="$catqt_build_rootdir"/gradle-properties
        catqt_androidmanifest="$catqt_plugin_androidsrc"/AndroidManifest.xml
        catqt_android_sdk_root="$catqt_plugin_androidhome"
        catqt_androidapistring=android-"$catqt_plugin_androidapiversion"
        catqt_android_makecmd="$catqt_plugin_androidndkroot"/prebuilt/"$androidndkhost"/bin/make
    fi

    set +x
}
