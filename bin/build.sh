#!/usr/bin/env bash

# --- note that java sources are not compiled until the deploy step.

set -eu
set -o pipefail

set +e
read -d '' USAGE <<EOT
Usage: $0
  --mode=init { -c config-file } { -b build-dir } { -d project-dir }

  --mode=make-android   --variant={ test | release }
  --mode=deploy-android --variant={ test | release } (implies make-android)

  --mode=make-desktop   --variant={ test | release }

Note that it might be necessary to set JAVA_HOME (e.g.
/usr/lib/jvm/java-8-openjdk-amd64)
EOT

_ret0=

bindir=$(realpath "$(dirname "$0")")

. "$bindir"/functions.bash

# --- cat-qt-webview root dir.
catqt_build_rootdir="$bindir"/..

os=
# --- lowercase os
los=
machine=

mode_init=
mode_make_desktop=
mode_make_android=
mode_deploy_android=

configfile=
# configfilevariant=
catqt_build_builddir=
catqt_build_projdir=

# --- test / release
catqt_build_variant=

buildfile="$bindir"/buildfile


# xxx
# # catqt_build_action=make
# catqt_build_target=android
# catqt_build_target=desktop
# catqt_build_target=ios

while getopts hc:b:d:-: arg; do
    case $arg in
        h) warn "$USAGE"; exit 0 ;;
        b) catqt_build_builddir="$OPTARG" ;;
        c) configfile="$OPTARG" ;;
        d) catqt_build_projdir="$OPTARG" ;;
        -) OPTARG_VALUE="${OPTARG#*=}"
            case $OPTARG in
                help)                warn "$USAGE"; exit 0 ;;
                variant=test)        catqt_build_variant=test;         ;;
                variant=release)     catqt_build_variant=release;      ;;
                mode=init)
                    mode_init=yes
                ;;
                mode=make-android)
                    catqt_build_target=android
                    catqt_build_action=make
                    mode_make_android=yes
                ;;
                mode=deploy-android)
                    catqt_build_target=android
                    catqt_build_action=deploy
                    mode_deploy_android=yes
                ;;
                mode=make-desktop)
                    catqt_build_target=desktop
                    catqt_build_action=make
                    mode_make_desktop=yes
                ;;
                '')    break ;;
                *)     error "Illegal option --$OPTARG"  ;;
            esac ;;
        *) error "$USAGE" ;;
    esac
done
shift $((OPTIND-1))

init-os () {
    cmd-capture _ret0 uname -s
    os="$_ret0"
    cmd-capture _ret0 do-sed -e 's/\(.\+\)/\L\1/' <<< "$os"
    los="$_ret0"
    info "$(printf "os: %s" $(yellow "$os"))"
}

init-machine () {
    cmd-capture _ret0 uname -m
    machine="$_ret0"
    info "$(printf "machine: %s" $(cyan "$machine"))"
}

do-sed () {
    if [ "$os" = Darwin ]; then
        gsed "$@"
    else
        sed "$@"
    fi
}

check-mode () {
    local modes=(
        "$mode_init"
        "$mode_make_android"
        "$mode_make_desktop"
        "$mode_deploy_android"
    )
    local joined
    join _ret0 modes ""
    joined=$_ret0
    if [ "$joined" = '' ]; then
        warn "Missing mode"
        error "$USAGE"
    fi
}

check-args-init () {
  if [ -z "$catqt_build_builddir" ]; then
      error "Missing build dir (-b)"
      error "$USAGE"
  fi
  if [ -z "$catqt_build_projdir" ]; then
      error "Missing project dir (-d)"
      error "$USAGE"
  fi

  # configfile="$catqt_build_projdir"/catqt-plugin-vars-main.sh

  if [ ! -f "$configfile" ]; then
      error "$(printf "Config file %s missing" "$(bright-red "$configfile")")"
  fi
  if [ ! -d "$catqt_build_builddir" ]; then
      cmd mkdir -p "$catqt_build_builddir"
  fi
  if [ ! -d "$catqt_build_projdir" ]; then
      error "$(printf "Project dir %s missing" "$(bright-red "$catqt_build_projdir")")"
  fi

  configfile=$(realpath "$configfile")
  catqt_build_builddir=$(realpath "$catqt_build_builddir")
  catqt_build_projdir=$(realpath "$catqt_build_projdir")
}

check-args-make () {
    if [ -z "${catqt_build_variant:+x}" ]; then
        warn "$(printf "Missing value for %s" "$(bright-red catqt_build_variant)")"
        error "$USAGE"
    fi
    if [ "$catqt_build_variant" != test -a "$catqt_build_variant" != release ]; then
        error "$(printf "Invalid value for variant: %s" "$(bright-red "$catqt_build_variant")")"
    fi
}

cmd-source-if-exists () {
  if [ -e "$1" ]; then
    fun cmd-source "$1"
  fi
}

load-config-main () {
  configfilelocalpre="$catqt_build_projdir"/catqt-plugin-vars-local-pre.sh
  fun cmd-source-if-exists "$configfilelocalpre"

  # --- catqt-plugin-vars-main.sh
  fun cmd-source "$configfile"

  configfilelocalonmain="$catqt_build_projdir"/catqt-plugin-vars-local-onmain.sh
  fun cmd-source-if-exists "$configfilelocalonmain"
}

check-build-target () {
    if [ -z "${catqt_build_target:+x}" ]; then
        warn "$(printf "Missing value for %s (check -local config)" "$(bright-red catqt_build_target)")"
        error "$USAGE"
    fi
    # --- ios xxx
    if [ "$catqt_build_target" != android -a "$catqt_build_target" != desktop ]; then
        error "$(printf "Invalid value for target: %s" "$(bright-red "$catqt_build_target")")"
    fi
}

infer-ndk-host () {
    local ret="$1"
    local str=$(printf "%s-%s" "$los" "$machine")
    read -d '' "$ret" <<< "$str" || true
}

set-android-ndk-host () {
  if [ -n "$catqt_plugin_androidndkhostoverride" ]; then
      androidndkhost="$catqt_plugin_androidndkhostoverride"
  else
      fun infer-ndk-host _ret0
      androidndkhost="$_ret0"
  fi
}

set-android-ndk-prefix () {
    if [ "$catqt_plugin_androidarch" = armeabi-v7a ]; then
        androidndktoolchainprefix=arm-linux-androideabi
        androidndktoolsprefix=arm-linux-androideabi
    elif [ "$catqt_plugin_androidarch" = x86 ]; then
        androidndktoolchainprefix=x86
        androidndktoolsprefix=i686-linux-android
    else
        error "Invalid android arch ($catqt_plugin_androidarch)"
    fi
}

load-config-build () {
  configfilelocalpost="$catqt_build_projdir"/catqt-plugin-vars-local-post.sh

  configfilevariant="$catqt_build_projdir"/catqt-plugin-vars-variant-"$catqt_build_variant".sh

  fun load-config-main
  fun check-build-target
  fun cmd-source-if-exists "$configfilevariant"
  fun cmd-source-if-exists "$configfilelocalpost"

  if [ "$catqt_build_target" = android ]; then
      fun set-android-ndk-host
      fun set-android-ndk-prefix
  fi
}

load-config-init () {
  fun load-config-main
}

load-catqt-vars () {
    cmd-source "$bindir"/vars.sh
}

load-catqt-vars-make () {
    fun load-catqt-vars
    fun catqt-vars-make
}

load-catqt-vars-init () {
    fun load-catqt-vars
    fun catqt-vars-init
}

init-build () {
    mkd "$catqt_build_builddir"
    safe-rm-dir-allow-absolute "$catqt_build_builddir" "$catqt_builddirstub"
    mkd "$catqt_builddirfull"
}

copy-files () {
    local i

    # --- qmake
    cmd cp -a "$catqt_qmakefilepri" "$catqt_builddirfull"
    cmd cp -a "$catqt_plugin_qmakefilepluginpri" "$catqt_builddirfull"
    cmd cp -a "$catqt_qmakefileproorig" "$catqt_projfile"

    # --- src main
    for i in "${catqt_srcfiles[@]}"; do
        cmd cp -a "$i" "$catqt_builddirfull"
    done
    # --- src foreign
    for i in "${catqt_plugin_srcfilesplugin[@]}"; do
        cmd cp -a "$i" "$catqt_builddirfull"
    done
}

error-theme () {
    error "$(printf 'Bad theme %s (check %s)' \
          "$(bright-red $catqt_plugin_theme)" \
          "$(yellow '$catqt_plugin_theme')")"
}

check-theme () {
    info "Checking for themedir $catqt_themedir"
    if [ ! -d "$catqt_themedir" ]; then error-theme; fi
}

do-theme-cat-spinner () {
    local i

    local tgtdir="$catqt_builddirresources"/theme
    local tgtdirrelative="$catqt_builddirresourcesstub"/theme
    mkd "$tgtdir"
    for i in small medium large; do
        cmd cp "$catqt_themedir"/cat-spinner-"$i".gif "$tgtdir"/animation-"$i".gif
        fun add-qrc-entry "$tgtdirrelative"/animation-"$i".gif
    done
}

init-builddir-resources () {
    mkd "$catqt_builddirresources"
}

init-qrc () {
    cpa "$catqt_qrcdirroot"/resources.qrc-template "$catqt_builddirfull"/resources.qrc
}

add-qrc-entry () {
    local entry=$1

    local indent=4
    local ind=''
    local i=0
    while [ "$i" -lt "$indent" ]; do
        ind+=' '
        let i=i+1 || true
    done

    local src="s,%CATQT_RESOURCE%,$ind<file>$entry</file>\n%CATQT_RESOURCE%,;"
    fun do-sed -i -e "$src" "$catqt_builddirfull"/resources.qrc
}

finish-qrc () {
    local src="s,%CATQT_RESOURCE%,,;"
    fun do-sed -i -e "$src" "$catqt_builddirfull"/resources.qrc
}

do-theme () {
    if [ "$catqt_plugin_theme" = cat-spinner ]; then
        fun do-theme-cat-spinner
    else error-theme; fi
}

# --- not doing the same kind of checking as with theme.

do-qml () {
    mci
    mcb redirect-out "$catqt_builddirresources"/main.qml
    mcb redirect-in "$catqt_qmldir"/main.qml
    mcb do-sed -e " \
        s,__CAT_URL__,$catqt_plugin_webviewlocation,;      \
        s,__CAT_WINDOW_TITLE__,$catqt_plugin_windowtitle,; \
        "
    mcg

    cpa "$catqt_plugin_projqmldir"/Plugin.qml "$catqt_builddirresources"

    fun add-qrc-entry "$catqt_builddirresourcesstub"/main.qml
    fun add-qrc-entry "$catqt_builddirresourcesstub"/Plugin.qml
}

do-resources () {
    chd "$catqt_plugin_projresourcesdir"
    local file="$catqt_builddirfull"/resources.tmp
    mci
    mcb redirect-out "$file"
    mcb find * -type f
    mcg || true
    local line
    while read line; do
        fun add-qrc-entry "$catqt_builddirresourcesstub"/"$line"
    done < "$file"

    cpa * "$catqt_builddirresources"
}

android-do-manifest () {
    mci
    mcb redirect-out "$catqt_builddirfull"/android-sources/AndroidManifest.xml
    mcb redirect-in "$catqt_androidmanifest"
    mcb do-sed -e " \
        s,%CATQT_MANIFEST_PACKAGE_NAME%,$catqt_plugin_manifestpackagename,; \
        s,%CATQT_MAINACTIVITY_PACKAGE_NAME%,$catqt_plugin_mainactivity_packagename,; \
        s,%CATQT_MAINACTIVITY_ICON_LABEL%,$catqt_plugin_androidmainactivityiconlabel,; \
        s,%CATQT_MAINACTIVITY_LIBNAME%,$catqt_plugin_projectname,; \
        "
    mcg
}

write-buildfile () {
    redirect-out "$buildfile" cat <<-EOT
	set -eu
	set -o pipefail
	configfile="$configfile"
	catqt_build_builddir="$catqt_build_builddir"
	catqt_build_projdir="$catqt_build_projdir"
EOT
}

mode-make-common () {
    cmd-source "$buildfile"
    fun load-config-build
    fun load-catqt-vars-make

    fun init-qrc
    fun init-builddir-resources
    fun do-theme
    fun do-qml
    fun do-resources
    fun finish-qrc
    fun copy-files

    # --- necessary? xxx
    xport QT_ACCESSIBILITY 1
    xport QT_LINUX_ACCESSIBILITY_ALWAYS_ON 1
}

# --- throw away the shadow build every time, and do the incremental make in
# the builddir.

get-qmake-cmd () {
    local ret="$1"
    local str
    if [ "$catqt_build_target" = android ]; then
        str="$catqt_qtqtdir"/android_armv7/bin/qmake
    elif [ "$catqt_build_target" = desktop ]; then
        str="$catqt_qtqtdir"/gcc_64/bin/qmake
    else
        error "Not implemented (get-qmake-cmd)"
    fi
    read -d '' "$ret" <<< "$str" || true
}

qmake-android () {
    local qmakecmd
    get-qmake-cmd _ret0
    qmakecmd="$_ret0"
    local uppervariant
    cmd-capture _ret0 do-sed -e 's/\(.\+\)/\U\1/' <<< "$catqt_build_variant"
    uppervariant="$_ret0"
    xport CAT_VARIANT "$uppervariant"
    cmd "$qmakecmd" "$catqt_qmakefileprobuild" -spec android-clang
}

qmake-desktop () {
    local qmakecmd
    get-qmake-cmd _ret0
    qmakecmd="$_ret0"
    cmd "$qmakecmd" "$catqt_qmakefileprobuild"
}

export-vars-android () {
    xport ANDROID_HOME "$catqt_plugin_androidhome"
    xport ANDROID_NDK_HOST "$androidndkhost"
    xport ANDROID_NDK_PLATFORM "$catqt_plugin_androidndkplatform"
    xport ANDROID_NDK_ROOT "$catqt_plugin_androidndkroot"
    xport ANDROID_NDK_TOOLCHAIN_PREFIX "$androidndktoolchainprefix"
    xport ANDROID_NDK_TOOLCHAIN_VERSION "$catqt_plugin_androidndktoolchainversion"
    xport ANDROID_NDK_TOOLS_PREFIX "$androidndktoolsprefix"
    xport ANDROID_SDK_ROOT "$catqt_android_sdk_root"
}

make-android () {
    local make="$catqt_android_makecmd"

    cmd "$make" qmake_all
    cmd "$make" -j
    cmd "$make" INSTALL_ROOT="$shadowbuilddir"/android-build install
}

make-desktop () {
    local make="$catqt_desktop_makecmd"
    cmd "$make" qmake_all
    cmd "$make" -j
    cmd "$make" INSTALL_ROOT="$shadowbuilddir"/binary install
}

copy-built-objects () {
    cmd cpa "$catqt_builddirfull"/*.{o,so} "$shadowbuilddir"
}

prepare-gradle () {
    local ab="$shadowbuilddir"/android-build
    local gwp="$ab"/gradle/wrapper
    cmd cpa "$catqt_androidsrc"/build.gradle "$ab"
    cmd mkdir -p "$gwp"
    cmd cpa "$catqt_gradleproperties"/gradle-slash-wrapper-slash-gradle-wrapper.properties "$gwp"/gradle-wrapper.properties
}

# --- because it seems they try to compile everything in the java sources
# regardless of package or file name, it is better to clear it every time.

clear-android-sources-target () {
    fun safe-rm-dir-allow-absolute "$catqt_builddirfull" android-sources
}

copy-android-sources () {
    local tgt="$catqt_builddirfull"/android-sources
    mkd "$tgt"
    cpa "$catqt_androidsrc"/* "$tgt"
    cpa "$catqt_plugin_androidsrc"/* "$tgt"
}

_android-apk () {
    local install="$1"

    local release=
    if [ "$catqt_build_variant" = release ]; then
        release=yes
    fi

    cmd cp -a "$catqt_builddirfull"/"$catqt_deployjson" "$shadowbuilddir"

    mci
    mcb cmd "$catqt_qtqtdir"/android_armv7/bin/androiddeployqt
    mcb   --input "$shadowbuilddir"/"$catqt_deployjson"
    mcb   --output "$shadowbuilddir"/android-build

    if [ "$release" = yes ]; then
        # --- not possible without signing
        mcb   --release
        mcb   --sign "$catqt_plugin_androidkeyurl" "$catqt_plugin_keyalias"
    fi

    if [ "$install" = yes ]; then
        # --- also uninstalls current
        mcb   --install
    fi

    # --- this is the sdk version used in building the app.
    # --- not sure if/how it corresponds to the target sdk for the app.
    # --- in any case the android manifest should have both a minimum and a
    # target value for sdk, and the plugin provides the manifest.
    mcb   --android-platform "$catqt_androidapistring"
    mcb   --jdk "$catqt_plugin_androidjdk"
    mcb   --gradle
    mcg
}

android-apk-make () {
    fun _android-apk no
}

android-apk-make-and-deploy () {
    fun _android-apk yes
}

check-init () {
    if [ ! -f "$buildfile" ]; then
        error "$(printf "No buildfile (%s) -- please run with --mode=init first." "$(bright-red "$buildfile")")"
    fi
}

get-qt-ver () {
    local ret="$1"
    local qmakecmd
    get-qmake-cmd _ret0
    qmakecmd="$_ret0"
    set +e
    read -d '' perl <<-'EOT'
        undef $/; $_ = <>;
        if (m, using \\s+ qt \\s+ version \\s+ ([\\d\\.]+) ,xi) {
            print $1; exit;
        }
        print 'UNKNOWN'
	EOT
    set -e

    cmd-capture _ret0 perl -e "$perl" <<< "$($qmakecmd -v)"
    ver="$_ret0"
    read -d '' "$ret" <<< "$ver" || true
}

prepare-shadow-build-dir () {
    local qtver
    get-qt-ver _ret0
    qtver="$_ret0"
    local tar
    local stub
    if [ "$catqt_build_target" = android ]; then
        tar=android-"$catqt_plugin_androidarch"
    elif [ "$catqt_build_target" = desktop ]; then
        tar=desktop-"$los"-"$machine"
    else
        error "not implemented (prepare-shadow-build-dir)"
    fi
    stub=build-"$catqt_plugin_projectname"-"$tar"-qt-"$qtver"-"$catqt_build_variant"
info "$stub"
    shadowbuilddir="$catqt_shadowbuilddirroot"/"$stub"
    fun safe-rm-dir-allow-absolute "$catqt_shadowbuilddirroot" "$stub"
}

# --- desktop builds do not currently cross-compile.
# --- we do not compile anything differently for making the c++ sources based on
# test / release -- we always leave off CONFIG += DEBUG, so in that sense
# it's always release
# --- but, the .qml files need to be prepared according to variant, or else
# the qrc step of the build will fail.

build () {
    if [ "$catqt_build_target" = android ]; then
        fun check-init
        fun check-args-make
        fun mode-make-common
        chd "$catqt_builddirfull"
        fun export-vars-android
        fun prepare-shadow-build-dir
        fun qmake-android
        fun make-android
        fun copy-built-objects
        cmd cp -a "$catqt_builddirfull"/"$catqt_deployjson" "$shadowbuilddir"

        fun prepare-gradle
        fun clear-android-sources-target
        fun copy-android-sources
        fun android-do-manifest

        if [ "$catqt_build_action" = make ]; then
            fun android-apk-make
        elif [ "$catqt_build_action" = deploy ]; then
            fun android-apk-make-and-deploy
        fi
    fi

    if [ "$catqt_build_target" = desktop ]; then
        if [ "$catqt_build_action" = make ]; then
            fun check-init
            fun check-args-make
            fun mode-make-common
            chd "$catqt_builddirfull"
            fun prepare-shadow-build-dir
            fun qmake-desktop
            fun make-desktop
        fi
    fi
}

fun init-os
fun init-machine
fun check-mode

if [ "$mode_init" ]; then
    fun check-args-init
    fun load-config-init
    fun load-catqt-vars-init

    fun init-build
    fun check-theme

    fun write-buildfile
else
    fun build
fi

info 'All done!'
