// --- for console.log to work, this must be in ~/.config/QtProject/qtlogging.ini:
//
// [Rules]
// *.debug=true
// qt.*.debug=false

// --- we use two kinds of timers, partly as a demo.
// Timer is a normal component (tick = onTriggered)
// QTimer is injected through C++ and is used here as a child of WebView (tick = onTimeout)

import QtMultimedia 5.8
import QtQml 2.2
import QtQuick 2.10
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2
import QtWebView 1.1

// import alleycat.qtimer 1.0
import alleycat.plugin 1.0

Window {
  id: root

  visible: true
  width: screenWidth
  height: screenHeight
  x: 0
  y: 0
  title: qsTr("__CAT_WINDOW_TITLE__")

  PluginRoot {
    id: pluginRoot
  }

  Item {
    id: pluginLoader
    // --- this alias is necessary in order to refer to the inner
    // properties from outside.
    // --- `item` is the first object loaded.
    property alias plugin: pluginLoaderLoader.item
    Loader {
      id: pluginLoaderLoader
      source: "Plugin.qml"
    }
  }

  Timer {
    id: splashtimer
    interval: 200
    running: true
    repeat: true

    // --- min time: roughly 2secs, bit more.
    // property int minticks: 25
    property int minticks: 0

    property int ticks: 0
    onTriggered: {
      ++this.ticks
      if (this.ticks >= this.minticks && webview.success) {
        splash.visible = false
        splash.height = 0
        this.running = false
        // xxx might be good to use anchors here (desktop version is
        // full-screen and doesn't resize).
        webview.width = screenWidth
        webview.height = screenHeight
        pluginLoader.plugin.onWebViewMaximized ()
      }
    }
  }

  ColumnLayout {
    id: splash
    anchors.fill: parent

    ProgressBar {
        id: webviewProgress
        value: webview.loadProgress / 100
        anchors.left: parent.left
        anchors.right: parent.right
    }

    // --- width & height are apparently ignored for image, hence this solution.
    AnimatedImage {
      // --- already in theme/
      // source: screenWidth < 500 ? "theme/animation-small.gif" : screenWidth < 700 ? "theme/animation-medium.gif" : "theme/animation-large.gif"
      source: screenWidth < 500 ? "theme/animation-small.gif" : "theme/animation-medium.gif"
      anchors.horizontalCenter: parent.horizontalCenter
      anchors.verticalCenter: parent.verticalCenter
    }
  }

  WebView {
    id: webview
    objectName: "webview"

    // --- url: set after js has been injected (initActivity)

    width: 0

    // --- achtung: using `visible` to control the splash logic will cause
    // the back button to silently fail forever.
    // visible: false

    focus: true

    property bool success: false
    property bool error: false

    Keys.onPressed: {
      pluginLoader.plugin.onKeyPressed (event, webview)
    }

    // Keys.onBackPressed: {
    // pluginLoader.plugin.onBackPressed ()
    // }

    Keys.onReleased: {
      pluginLoader.plugin.onKeyReleased (event, webview)

      if (event.key == Qt.Key_Back) {
        pluginLoader.plugin.onBackPressed (event, webview)
        event.accepted = true

        // --- checking the property is probably redundant on android.
        if (this.canGoBack) this.goBack ()
      }
    }

    /*
    QTimer {
      id: timer
      interval: 1000
      onTimeout: {
        webview.runJavaScript ("\
          function json (x) { return JSON.stringify (x); } \
          ;( function () { \
            var wh = window ['wormhole-half']; \
            if (!wh) return json ({}); \
            var ret = json ({ notify: wh.get () }); \
            wh.clear (); \
            return ret; \
          } ()); \
        ",
        function (result) {
          var notify = JSON.parse (result).notify
          if (!notify) return
          notify.forEach (function (x) {
            var ok
            if (x.show) {
              var text = x.show.text || '[unknown text]'
              var tag = x.show.tag || '[unknown tag]'
              var id = x.show.id || -1
              ok = foreignRoot.notify (text, tag, id)
              if (!ok) console.error ("Couldn't notify")
            }
          })
        })
      }
    }
    */

    onLoadingChanged: {
      var status = loadRequest.status

      if (status === WebView.LoadStartedStatus) {
        pluginLoader.plugin.onWebViewLoadStarted (loadRequest, webview)
      }
      else if (status === WebView.LoadFailedStatus) {
        var errorCode = loadRequest.errorCode
        var errorString = loadRequest.errorString
        console.log ('webview: load failed for url: ' + this.url)

        // --- doesn't exist on the desktop version.
        if (NetworkReply) {
          if (errorCode === NetworkReply.OperationCanceledError) {
            pluginLoader.plugin.onWebViewLoadCancelled (loadRequest, webview)
            console.log ('webview: loading cancelled');
          } else {
            pluginLoader.plugin.onWebViewLoadFailed (errorCode, errorString, loadRequest, webview)
            console.log ('webview: load failed (' + errorString + ')');
          }
        }
        this.error = true

        // @todo xxx show error
      }
      else if (status === WebView.LoadSucceededStatus) {
        pluginLoader.plugin.onWebViewLoadSucceeded (loadRequest, webview)
        console.log ('calling page loaded')

        // --- this is where zoom out happens.
        // --- timing is a bit tricky -- it could still be too early, as is
        // the case with passphrases.
        pluginRoot.pageLoaded ()

        this.success = true
      }
      else {
        pluginLoader.plugin.onWebViewLoadUnknown (loadRequest, webview)
        console.log ('webview: loading changed: unknown status, errorString: ' + loadRequest.errorString);
        this.error = true

        // @todo xxx show error
      }
    }

    Component.onCompleted: function () {
      pluginLoader.plugin.onWebViewComponentCompleted ()

        // --- set url (if desired) after initting activity, so the js interface works.

        var ret = pluginRoot.initActivity ('__CAT_URL__')
        var ok = ret.ok
        if (!ok) return console.log ("Couldn't init activity.")
        var url = ret.url
        if (url) webview.url = url
    }
  }
}
