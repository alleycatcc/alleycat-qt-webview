#ifndef CAT_GLOBAL_H
#define CAT_GLOBAL_H

#include <QString>

#define CAT_ERROR(x) do {           \
    std::cerr << ((QString) (   \
        x                       \
    )).toStdString() << std::endl; \
} while(0);

#define QML_MAIN "qrc:/resources/main.qml"

#define LOG_TAG "PassphrasesActivity"
void catqt_debug(QString s);

struct catqt_jniSpec {
    const char *javaClass;
    const char *javaFuncName;
    const char *signature;
    void *cppFunc;
};

#endif
