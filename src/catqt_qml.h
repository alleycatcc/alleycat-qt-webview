#ifndef CAT_QML_H
#define CAT_QML_H

#include <QObject>
#include <QtQml/QQmlContext>
#include <QQmlProperty>

void catqt_qml_init(QQmlContext *contextArg);
void catqt_qml_setRootWindow(QObject *rootWindowArg);

void catqt_qml_setQMLPropertyViaContext(const QString &name, const QVariant &value);
void catqt_qml_setQMLPropertyViaContext(const QString &name, QObject *value);

void catqt_qml_setQMLPropertyOnRootWindow(const QString &name, const QVariant &value);

void catqt_qml_setQMLPropertyOnObject(QObject *object, const QString &name, const QVariant &value);
void catqt_qml_setQMLPropertyOnChildOfRootWindow(const QString &objName, const QString &name, const QVariant &value);

#endif
