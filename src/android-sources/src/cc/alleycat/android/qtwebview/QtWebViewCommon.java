package cc.alleycat.android.qtwebview;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.webkit.WebView;

public class QtWebViewCommon {
    private static final String TAG = "QtWebViewCommon";

    public static void pageLoaded(Activity activity, WebView webview) {
        final Runnable fix = fixZoom(webview);

        // --- doing it as a post does not fix the timing issue with file:
        // urls.
        scheduleUiNow(activity, fix);
        // scheduleUiPost(fix);
    }

    public static void scheduleUiNow(Activity activity, Runnable runnable) {
        activity.runOnUiThread(runnable);
    }

    // --- always waits for one context switch, in contrast to
    // `scheduleUiNow`.

    public static void scheduleUiPost(Runnable runnable) {
        Looper looper = Looper.getMainLooper();
        Handler handler = new Handler(looper);
        handler.post(runnable);
    }

    private static Runnable fixZoom(final WebView webview) {
        return new Runnable() {
            public void run() {
                for (int i = 0; i < 10; i++) {
                    boolean zoomed = webview.zoomOut();
                    Log.w(TAG, String.format("zoom out (%d) successful: %b", i, zoomed));
                }
            }
        };
    }
}

