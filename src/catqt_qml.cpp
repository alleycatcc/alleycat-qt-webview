#include "catqt_global.h"
#include "catqt_qml.h"

QQmlContext *context;
QObject *rootWindow;

void catqt_qml_init(QQmlContext *contextArg)
{
    context = contextArg;
}

void catqt_qml_setRootWindow(QObject *rootWindowArg)
{
    rootWindow = rootWindowArg;
}


// --- set a property at the base level of the qml.
// --- in general it's more useful to set them on objects though.

void catqt_qml_setQMLPropertyViaContext(const QString &name, QObject *value)
{
    if (!context) {
        catqt_debug(QString("tried to set property %1 on null context").arg(name));
        return;
    }
    context->setContextProperty(name, value);
}

void catqt_qml_setQMLPropertyViaContext(const QString &name, const QVariant &value)
{
    if (!context) {
        catqt_debug(QString("tried to set property %1 on null context").arg(name));
        return;
    }
    context->setContextProperty(name, value);
}

void catqt_qml_setQMLPropertyOnObject(QObject *object, const QString &name, const QVariant &value)
{
    QQmlProperty prop(object, name);

    // --- to read:
    // QVariant v = prop.read();

    bool ok = prop.write(value);
    if (!ok) catqt_debug(QString("couldn't set prop %1").arg(name));
}

// --- the object must be a child of rootWindow.
void catqt_qml_setQMLPropertyOnChildOfRootWindow(const QString &objName, const QString &name, const QVariant &value)
{
    if (!rootWindow) {
        catqt_debug(QString("catqt_qml_setQMLPropertyOnChildOfRootWindow(): Can't get root window"));
        return;
    }
    QObject *obj = rootWindow->findChild<QObject*>(objName);
    if (!obj) {
        catqt_debug(QString("setObjectProperty(): Can't find object with name %1").arg(objName));
        return;
    }
    catqt_qml_setQMLPropertyOnObject(obj, name, value);
}

void catqt_qml_setQMLPropertyOnRootWindow(const QString &name, const QVariant &value)
{
   if (!rootWindow) {
       catqt_debug(QString("catqt_qml_setQMLPropertyOnRootWindow(): tried to set property %1 on null root window").arg(name));
       return;
   }
   catqt_qml_setQMLPropertyOnObject(rootWindow, name, value);
}


