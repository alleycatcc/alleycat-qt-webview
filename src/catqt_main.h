#ifndef CAT_MAIN_H
#define CAT_MAIN_H

#include <vector>

#include <QGuiApplication>
#include <QScreen>
#include <QStyleHints>
#include <QTimer>
#include <QDebug>

#include <QQmlApplicationEngine>
#include <QQmlProperty>
#include <QtQml/QQmlContext>
#include <QtWebView/QtWebView>

#ifndef PLATFORM_ANDROID
// # include <QWebEngineView>
// # include <QWebChannel>
#endif

#include "catqt_global.h"
#include "catqt_foreign.h"

#endif
