#include <vector>
#include <iostream>
#include <tuple>
#include <map>

#ifdef PLATFORM_ANDROID
# include <android/log.h>
# include <QtAndroidExtras/QAndroidJniEnvironment>
# include <QtAndroidExtras/QAndroidJniObject>
# include <jni.h>
#endif

#include "catqt_qml.h"

#include "catqt_main.h"

using std::vector;
using std::tuple;
using std::pair;
using std::map;

QRect scaleAndCenter(double scale);
QRect scaleAndCenter(int width, int height);
QRect _scaleAndCenter(double a, double b);
QRect getScreenDimensions();

CATQT_FOREIGN_ROOT *foreignRoot = nullptr;

// --- level 5 is warn.
void catqt_debug(QString s)
{
    qDebug() << s;
#ifdef PLATFORM_ANDROID
    __android_log_write(5, LOG_TAG, s.toLatin1().data());
#endif
}

// --- from ndk docs.
// --- use 1.6 (only example in the docs)
// --- this is a callback which is fired when the jni is ready and is thus
// the right place to register the native functions.
// --- note that we are bypassing the QtAndroidExtras here and just doing it
// the ndk way.
// --- the Qt way wasn't totally clear.

#ifdef PLATFORM_ANDROID
JNIEXPORT jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
    Q_UNUSED(reserved);

    JNIEnv *env;

    if (vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK)
        return JNI_ERR;

    int numMethods = sizeof(catqt_foreign_jniSpec) / sizeof(catqt_foreign_jniSpec[0]);

#define U const char *, const char *, void *
#define W tuple<U>
#define V const char *, vector<W> *

    map<V> classMap;

    for (int i = 0; i < numMethods; i++) {
        struct catqt_jniSpec spec = catqt_foreign_jniSpec[i];

        tuple<U> tup = std::make_tuple(
            spec.javaFuncName,
            spec.signature,
            spec.cppFunc
        );
        const char *javaClass = spec.javaClass;

        vector<W> *vec;
        auto search = classMap.find(javaClass);
        if (search == classMap.end()) {
            vec = new vector<W>();
            classMap.insert(std::pair<V>(javaClass, vec));
        }
        else {
            vec = search->second;
        }
        vec->push_back(tup);
    }

    int errors = 0;

    auto registerFunctions = [&errors, &env] (pair<V> p) {
        auto javaClass = p.first;
        auto vec = p.second;
        int numMethods = vec->size();

        JNINativeMethod methods[numMethods];
        int i = -1;
        auto insertFunction = [&i, &methods] (W tup) {
            methods[++i] = {std::get<0>(tup), std::get<1>(tup), std::get<2>(tup)};
        };

        std::for_each(vec->begin(), vec->end(), insertFunction);

        jclass theClass = env->FindClass(javaClass);

        if (!theClass) {
            ++errors;
            catqt_debug(QString("Couldn't find java class %1").arg(javaClass));
            return;
        }

        if (env->RegisterNatives(theClass, methods, numMethods) < 0) {
            ++errors;
            catqt_debug(QString("Couldn't register natives"));
            return;
        }
    };

    std::for_each(classMap.begin(), classMap.end(), registerFunctions);

    if (errors) return JNI_ERR;

    return JNI_VERSION_1_6;
}
#endif

// --- note: JNI_OnLoad happens asynchronously, probably somewhere after
// engine.load.

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    // --- has to come immediately after QGuiApplication is created.
    QtWebView::initialize();

    QQmlApplicationEngine engine;

    catqt_qml_init(engine.rootContext());

    foreignRoot = new CATQT_FOREIGN_ROOT(&engine);

    {
        QRect rect = getScreenDimensions();
        catqt_debug(QString("Screen size is %1 x %2").arg(rect.width()).arg(rect.height()));
        catqt_qml_setQMLPropertyViaContext(QStringLiteral("screenWidth"), rect.width());
        catqt_qml_setQMLPropertyViaContext(QStringLiteral("screenHeight"), rect.height());
    }

    // --- register custom components for use in QML.
    qmlRegisterType<QTimer>("alleycat.qtimer", 1, 0, "QTimer");
    qmlRegisterType<CATQT_FOREIGN_ROOT>("alleycat.plugin", 1, 0, "PluginRoot");

    engine.load(QUrl(QStringLiteral(QML_MAIN)));

    QList<QObject *> rootObjects = engine.rootObjects();
    if (rootObjects.isEmpty())
        return -1;

    catqt_qml_setRootWindow(rootObjects[0]);

    return app.exec();
}

QRect scaleAndCenter(int width, int height)
{
    return _scaleAndCenter(width, height);
}

QRect scaleAndCenter(double scale)
{
    return _scaleAndCenter(scale, 0);
}

QRect _scaleAndCenter(double a, double b)
{
    QRect screenGeometry = QGuiApplication::primaryScreen()->availableGeometry();
    qDebug() << "screen query:" << screenGeometry;
    QSize size;
    if (QGuiApplication::styleHints()->showIsFullScreen()) {
        size = screenGeometry.size();
    }
    else {
        if (b == 0) size = screenGeometry.size() * a;
        else size = QSize(a, b);
    }
    const QSize offset = (screenGeometry.size() - size) / 2;
    const QPoint pos = screenGeometry.topLeft() + QPoint(offset.width(), offset.height());
    return QRect(pos, size);
}

QRect getScreenDimensions()
{
    return QGuiApplication::primaryScreen()->availableGeometry();
}
