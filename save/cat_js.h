/**
 * This is for the 'full wormhole' (2-way communication between c++ and js
 * on platforms which support it.
 */

#ifndef CAT_JS_H
#define CAT_JS_H

#include <QObject>
#include <QString>
#include <QVariantMap>

#include "cat_top.h"

// --- forward declare -- fix circular dependency.
class CatTop;

class CatJs : public QObject
{
    Q_OBJECT

public:
    CatJs(CatTop *top);

// --- will be exported as js functions.
public slots:
    // void ping(QVariantMap arg);
    void savePreferences(QVariantMap arg);
    void openLink(QVariant s_url);
    void quit();
    // void notify();

private:

protected:
    CatTop *top;
};

#endif
