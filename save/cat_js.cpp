#include <iostream>

#include "cat_js.h"

CatJs::CatJs(CatTop *top) : top(top)
{
}

// --- QMap<QString, QVariant>
// void CatJs::ping(QVariantMap arg)
// {
//     QString name = arg["name"].toString();
//     QString type = arg["type"].toString();
//
//     std::cout << "ping"
//         << " " << name.toStdString()
//         << " " << type.toStdString()
//         << std::endl;
// }

// --- QMap<QString, QVariant>
void CatJs::savePreferences(QVariantMap arg)
{
    QString name = arg["name"].toString();
    QString type = arg["type"].toString();

    std::cout << "saving preferences:"
        << " " << name.toStdString()
        << " " << type.toStdString()
        << std::endl;

    top->savePreferences();
}

void CatJs::openLink(QVariant s_url)
{
    QString url = s_url.toString();
    top->openLink(url);
}

void CatJs::quit()
{
    top->quit();
}

// void CatJs::notify()
// {
//     top->notify();
// }
