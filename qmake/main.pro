TEMPLATE = app

QT += qml quick webview widgets

android: {
    QT += androidextras
    ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android-sources
    DISTFILES += android-sources/AndroidManifest.xml
}
!android: {
    # QT += webchannel webenginewidgets
    # QT += webchannel
}

CONFIG += c++11 console

HEADERS += catqt_global.h catqt_main.h   catqt_qml.h
SOURCES +=                catqt_main.cpp catqt_qml.cpp

# OTHER_FILES += \ qml/main.qml

RESOURCES += resources.qrc

android: {
    # --- instruct androiddeployqt to copy all the files to
    # <shadow-build>/android-build/assets
    catqt_plugin_deploy_webroot.files += resources/*
    catqt_plugin_deploy_webroot.path += /assets
    INSTALLS += catqt_plugin_deploy_webroot

    DEFINES += PLATFORM_ANDROID
    OTHER_FILES += android-sources/src/cc/alleycat/android/qtwebview/QtWebViewCommon.java
}

DEFINES += VARIANT_$$(CAT_VARIANT)

QML_IMPORT_PATH =

include(deployment.pri)

include(foreign.pri)
